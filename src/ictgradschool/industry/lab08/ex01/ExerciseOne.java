package ictgradschool.industry.lab08.ex01;

import java.io.*;

public class ExerciseOne {



    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        File myFile = new File("input2.txt");
        try (FileReader fR = new FileReader(myFile)){
            int num = fR.read();
            while (num != -1){
                if ((char)num == 'e'|| (char)num == 'E'){
                    System.out.println((char)num);
                    numE++;
                }
                total++;
                num = fR.read();
            }
//            System.out.println("The toal number of characters is: " + total);
//            System.out.println("The total number of \"E\"s is: " + numE);
        } catch(IOException e) {
            System.out.println("IO problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        try (BufferedReader bF = new BufferedReader(new FileReader("input2.txt"))){
            String s = bF.readLine();
            while (s != null){
                for (int i = 0; i < s.length(); i++){
                    if (s.charAt(i) == 'e' || s.charAt(i) == 'E'){
                        numE++;
                    }
                }
                total += s.length();
                s = bF.readLine();
            }

        }
        catch (IOException e){
            System.out.println("IO Problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}

package ictgradschool.industry.lab08.ex01;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Reader {

    public void start(){
        fileReaderEx01();
    }

    private void fileReaderEx01() {
        int num = 0;
        //FileReader fR = null;
        File myFile = new File("input1.txt");
        try (FileReader fR = new FileReader("input1.txt")){
            num = fR.read();
            System.out.println((char)num);
            System.out.println((char)fR.read());
            System.out.println((char)fR.read());
            System.out.println((char)fR.read());
            System.out.println((char)fR.read());

        } catch(IOException e) {
            System.out.println("IO problem");
        }
    }

    public static void main(String[] args) {

        Reader ex = new Reader();
        ex.start();

    }
}

package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import javax.sound.sampled.Line;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
        String myFile = Keyboard.readInput();
        File file  = new File(myFile);

        if (myFile != null) {
            try ( BufferedReader bF = new BufferedReader(new FileReader(file))){

                String line = bF.readLine();
                while (line!=null){
                    System.out.println(line);
                    line = bF.readLine();
                }
            }
            catch (IOException e){
                System.out.println("IO Error");
            }
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}

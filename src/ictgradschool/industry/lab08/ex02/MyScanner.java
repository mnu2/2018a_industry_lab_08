package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import javax.sound.sampled.Line;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
        String myFile = Keyboard.readInput();
        File file  = new File(myFile);
        try (Scanner scanner = new Scanner(file)){
            while (scanner.hasNextLine()){
                System.out.println(scanner.nextLine());
            }
        }
        catch(IOException e){
            System.out.println("IO Error");
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}

package ictgradschool.industry.lab08.ex04;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))){
            boolean foo = true;
            printWriter.write(films.length + ",");
            int i = 0;
            while (i < films.length){
                printWriter.write(films[i].getName() +"," );
                printWriter.write(films[i].getYear()+"," );
                printWriter.write(films[i].getLengthInMinutes()+"," );
                printWriter.write(films[i].getDirector()+"," );
                i++;
            }
        }
        catch (IOException e){
            System.out.println("IO Error");
        }

    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}

package ictgradschool.industry.lab08.ex04;

import com.sun.org.apache.xerces.internal.impl.xs.SchemaNamespaceSupport;
import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import javax.print.attribute.standard.JobOriginatingUserName;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        File myFile = new File(fileName);
        try (Scanner scanner = new Scanner(myFile)){
            scanner.useDelimiter(",|\\r\\n");
            Movie[] movie1 = new Movie[scanner.nextInt()];
            int i = 0;
            while (i < movie1.length){
                movie1[i] = new Movie(scanner.next(), scanner.nextInt(), scanner.nextInt(), scanner.next());
                i++;
            }
            return movie1;

        }
        catch(IOException e){
            System.out.println("IO Error: " + e.getMessage());
        }

            return null;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
